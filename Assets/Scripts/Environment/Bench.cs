using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Script to add functionality to bench
public class Bench : MonoBehaviour
{
    private bool firstSpotEmpty = true; // is the first spot free or busy?
    private bool secondSpotEmpty = true; // is the second spot free or busy?
    [SerializeField]
    private Transform firstSpot; // position of the first spot of the bench
    [SerializeField]
    private Transform secondSpot; // position of the second spot of the bench

    // checks if there is any empty spot in the bench
    public bool isEmpty()
    {
        return firstSpotEmpty || secondSpotEmpty;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Elder")) // check if an elder entered the collider
        {
            ElderStateMachine esm = other.GetComponent<ElderStateMachine>(); // get the state machine of the elder
            if (esm.tired) // check if the elder is tired
            {
                if (isEmpty()) // check it there is any empty spot on the bench
                {
                    int spot = occupyBench(); // get the bench spot where the elder will sit
                    if (spot == 1)
                    {
                        esm.agent.destination = firstSpot.position;
                        esm.goRest(this, spot);
                    } else if (spot == 2)
                    {
                        esm.agent.destination = secondSpot.position;
                        esm.goRest(this, spot);
                    }
                }
            }
        }
    }

    // Method to check if any spot is free and occupy it
    int occupyBench()
    {
        if(firstSpotEmpty)
        {
            firstSpotEmpty = false;
            return 1;
        } else if(secondSpotEmpty)
        {
            secondSpotEmpty = false;
            return 2;
        }
        return 0;
    }
}
