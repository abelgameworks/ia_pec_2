using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Script to get the next position the Elder will go based on a index they carry.
public class WanderableArea : MonoBehaviour
{
    public Transform entrance; // Entrance of the area
    public Transform exit; // Exit of the area
    public Transform sideA; // One of the two ways to go
    public Transform sideB; // The other way

    // Returns the next point the elder has to go to
    public Transform getNextPoint(int index)
    {
        if(index == 0)
        {
            return entrance;
        } 
        else if(index == 2)
        {
            return exit;
        }
        else
        {
            // Randomly goes from one of the two ways
            int i = Random.Range(0, 2);
            if (i == 0)
                return sideA;
            else
                return sideB;
        }
    }
}
