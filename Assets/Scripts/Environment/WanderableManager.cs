using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

// Script to manage the OffMeshLink through its trigger collider, this class is used to avoid that the runners bump into the elders when crossing
public class WanderableManager : MonoBehaviour
{
    OffMeshLink link; // reference to the offmeshlink managed
    bool runnerCrossing = false; // is a runner crossing the link?
    public List<ElderStateMachine> stoppedElders; // list referencing the stopped elders

    private void Start()
    {
        link = GetComponent<OffMeshLink>();
        stoppedElders = new List<ElderStateMachine>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Elder")) // if the object that enters in the collider is an elder, check if he crosses or stops
        {
            if (!runnerCrossing) // if no runner is crossing, let the elder cross and deactivate the elder so the runners dont bump them
            {
                link.activated = false;
            } 
            else // if there is a runner crossing, stop the elder and add it to the list of elders waiting to cross
            {
                ElderStateMachine esm = other.GetComponent<ElderStateMachine>();
                stoppedElders.Add(esm);
                esm.agent.speed = 0;
            }
        }
        
        if(other.CompareTag("Runner")) // if the object that enters the collider is a runner, mark that the area is being crossed by one
        {
            runnerCrossing = true;
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Elder")) // if the elder leaves the colldier, resume the normal behaviour of the link
        {
            link.activated = true;
        }
        
        if (other.CompareTag("Runner")) // if a runner leaves, check if there are elders stopped and let them walk again
        {
            runnerCrossing = false;
            if (stoppedElders != null && stoppedElders.Count > 0)
            { 
                foreach(ElderStateMachine esm in stoppedElders)
                {
                    esm.agent.speed = esm.walkingSpeed;
                }
                stoppedElders = new List<ElderStateMachine>();
            }
        }
    }
}
