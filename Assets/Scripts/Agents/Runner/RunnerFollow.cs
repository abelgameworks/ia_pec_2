using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RunnerFollow : MonoBehaviour
{
    public NavMeshAgent agent;  //reference to the NavMeshAgent component
    public NavMeshAgent ghost;  //reference to the NavMeshAgent component

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.speed = ghost.speed - 1.5f;
    }

    // Update is called once per frame
    void Update()
    {
        agent.destination = ghost.gameObject.transform.position;
    }
}
