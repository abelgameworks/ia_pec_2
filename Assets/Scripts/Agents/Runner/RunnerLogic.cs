using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

// Class for controlling the runner agents
public class RunnerLogic : MonoBehaviour
{
    public NavMeshAgent agent;  //reference to the NavMeshAgent component
    public GameObject destination; // Empty wayward object where the agent is heading
    private bool moving; // is the agent moving?
    public bool clockWise; // is it going clockwise or anti-clockwise?
    public int nextArea; // area of the plaza where the agent is going
    public int speed = 8; // base agent speed
    public int speedVariation = 2; // speed variation +/-
    public RunnerRoute route; // Reference to the object containing the wayward points
    public bool MoveAcrossNavMeshesStarted = false; // is the agent currently moving across a offmeshlink?

    // In the awake method, the agent starts moving at a random speed between a range, in a random direction and to a random point in that direction
    private void Awake()
    {
        moving = true;
        agent.speed = Random.Range(speed-speedVariation, speed+speedVariation);
        GetDirection();
        getNextPoint();
    }

    // Called each frame
    void Update()
    {
        // If the agent is currently moving through an OffMeshLink or if it isn't
        if (agent.isOnOffMeshLink && !MoveAcrossNavMeshesStarted)
        {
            StartCoroutine(MoveAcrossNavMeshLink());
            MoveAcrossNavMeshesStarted = true;
        }
        else
        {
            // If the distance between the agent and the next point is lesser than 3. get the next point where it'll go
            if (Vector3.Distance(gameObject.transform.position, destination.transform.position) < 8)
            {
                moving = false;
            }

            if (!moving)
            {
                moving = true;
                getNextPoint();
            }
        }
     }

    // Gets a random digit to determine if the agent is going clockwise or not
    void GetDirection()
    {
        int direction = Random.Range(0, 1); // Generates a random int that will be 0 or 1, if 0, the agent goes clockwise
        if (direction == 0)
        {
            clockWise = true;
        }
        else
        {
            clockWise = false;
        }
    }

    // Gets the next point where the agent will go and prepares everything to calculate the next point
    private void getNextPoint()
    {
       destination = route.areas[nextArea].getRandomPoint(); // Uses the RunnerRoute object and the Area objects inside it to get a random point in the corresponding part where the agent has to go

        // Increase or decrease where it will go accordingly
        if (clockWise)
            nextArea++;
        else
            nextArea--;

        // Make the movement circular
        if(nextArea >= route.areas.Count)
        {
            nextArea = 0;
        } else if (nextArea < 0)
        {
            nextArea = route.areas.Count-1;
        }

        // Make the agent go to the next point
        agent.destination = destination.transform.position;
    }

    // Method for crossing an OffMeshLink without using the normalized vector for speed like in Unity's Library
    IEnumerator MoveAcrossNavMeshLink()
    {
        OffMeshLinkData data = agent.currentOffMeshLinkData; // Gets the OffMeshLinkData to be used
        agent.updateRotation = false; // Stop rotating the capsule so it goes straight

        Vector3 startPos = agent.transform.position; // Starting position of the agent
        Vector3 endPos = data.endPos + Vector3.up * agent.baseOffset; // Position where the agent will finish
        float duration = (endPos - startPos).magnitude / agent.velocity.magnitude; // Calculate the transition duration using the speed and positions of the agent
        float t = 0.0f; // Timer control variable
        float tStep = 1.0f / duration; // Increase for the timer
        OffMeshLink link = data.offMeshLink.gameObject.GetComponent<OffMeshLink>(); // Reference to the OffMeshLink component
        link.activated = false; // Turn off the OffMeshLink

        // Loop until the time objective is reached
        while (t < 1.0f)
        {
            transform.position = Vector3.Lerp(startPos, endPos, t);
            agent.destination = transform.position;
            t += tStep * Time.deltaTime;
            yield return null;
        }

        link.activated = true; // Reactivate the OffMeshLink
        transform.position = endPos; // Update agent position
        agent.updateRotation = true; // Reactivate the capsule rotation
        agent.CompleteOffMeshLink(); // Mark the OffMeshLink transition as completed with Unity's method
        MoveAcrossNavMeshesStarted = false; // Mark the object as not crossing an OffMeshLink
        agent.destination = destination.transform.position; // Set the destination where the agent was going again
    }
}
