using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Scripts that controls every member of the formation
/// </summary>
public class Formation : MonoBehaviour
{
    private NavMeshAgent agent;
    public GameObject target; // Leader of the formation
    public Vector3 pos; // Position to be spawned in

    private Vector3 diff; // Gets the difference vector

    // Start is called before the first frame update
    void Start()
    {
        agent = gameObject.GetComponent<NavMeshAgent>();
        agent.updateRotation = false; // Disables the automatic rotation of the agent
        diff = GetDiff(); // Gets the difference vector
        transform.rotation = target.transform.rotation; // Looks the same way as the leader
        transform.position = target.transform.TransformPoint(pos); // Ubicates itself respective to the leader
        agent.destination = transform.position; // First destination
    }

    // Gets the Vector with the difference between the leader position and this game object
    private Vector3 GetDiff()
    {
        return target.transform.position - transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 newPos = GetDiff(); // Calculates the new difference
        // Checks the rotation
        if (agent.gameObject.transform.rotation != target.transform.rotation)
        {
            agent.gameObject.transform.rotation = target.transform.rotation;
        }

        // Checks if it has to move
        if (diff != newPos)
        {
            agent.destination = target.transform.position - diff;
        }
    }
}
