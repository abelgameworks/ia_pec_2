using Pada1.BBCore;
using UnityEngine;

namespace BBUnity.Conditions
{
    /// <summary>
    /// It is a perception condition to check if the objective is close depending on a given distance.
    /// outputs to true if the target is close enough to steal from
    /// </summary>
    [Condition("MyActions/Conditions/IsTargetCloseCustom")]
    [Help("Checks whether a target is close depending on a given distance and a second, closer distance")]
    public class IsTargetCloseCustom : GOCondition
    {
        ///<value>Input Target Parameter to to check the distance.</value>
        [InParam("target")]
        [Help("Target to check the distance")]
        public GameObject target;

        ///<value>Input maximun distance Parameter to consider that the target is close.</value>
        [InParam("closeDistance")]
        [Help("The maximun distance to consider that the target is close")]
        public float closeDistance;

        [InParam("bodyDistance")]
        [Help("The maximun distance to consider that the target is close enough to perform another action")]
        public float stealDistance;

        [OutParam("closeEnough")]
        [Help("Is it close enough to perform another action?")]
        private bool closeEnough = false;

        /// <summary>
        /// Checks whether a target is close depending on a given distance,
        /// calculates the magnitude between the gameobject and the target and then compares with the given distance.
        /// </summary>
        /// <returns>True if the magnitude between the gameobject and de target is lower that the given distance.</returns>
        public override bool Check()
        {
            bool close = (gameObject.transform.position - target.transform.position).sqrMagnitude < closeDistance*closeDistance;
            if (close)
            {
                if (closeEnough)
                    return false;
                if ((gameObject.transform.position - target.transform.position).sqrMagnitude < stealDistance*stealDistance)
                {
                    closeEnough = true;
                    return false;
                }
            } else
            {
                if (closeEnough)
                    closeEnough = false;
            }
            return close;
        }
    }
}