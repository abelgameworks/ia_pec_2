using Pada1.BBCore;
using Pada1.BBCore.Tasks;
using Pada1.BBCore.Framework;

[Action("MyActions/Thief/Steal")]
[Help("Tries to perform a steal")]
public class Steal : BasePrimitiveAction
{
	[OutParam("stealAttempted")]
    [Help("Is the steal already been tried?")]
	private bool stealAttempted;

	public override TaskStatus OnUpdate()
	{
		stealAttempted = true;
		return base.OnUpdate();
	}
}
