using Pada1.BBCore;
using Pada1.BBCore.Framework;
using UnityEngine;

namespace BBCore.Conditions
{
    /// <summary>
    /// Checks if there is a Thief alive through the Thief Manager.
    /// </summary>
    [Condition("MyActions/Conditions/CheckThiefSpawn")]
    [Help("Checks whether two booleans are true")]
    public class CheckThiefSpawn : ConditionBase
    {

        [InParam("ThiefManager")]
        [Help("Reference to the Thief Manager GameObject")]
        public ThiefManager ThiefManager;

        [InParam("TargetThief")]
        [Help("Thief currently targeted by this policeman")]
        public GameObject target;

        public override bool Check()
        {
            if (target != null)
                return false;

            Debug.Log("There isn't a target");

            return ThiefManager.isThiefAlive;
        }
    }
}
